안녕하세요! 42서울에서 공부 중인 카뎃입니다 :D  
Hello! I am a cadet studying in 42Seoul:D  
  
여기에 공개된 코드를 그대로 업로드하면 부정행위, 포인트 잃기 등의 불이익을 받게 됩니다.  
반드시 공부 목적으로만 참고해주세요.  
If you upload the code that is released here, you will be disadvantaged such as cheating and losing points.  
Please refer to it only for study purposes.  
